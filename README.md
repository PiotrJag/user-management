﻿# User Management
This application is a simple multilingual user management application. You can add, delete, edit and search users.

## Screenshots
![](./screenshots/screen0.png)

![](./screenshots/screen1.png)

## Technologies

* Java,
* JSP,
* Servlets,
* MySQL,
* [Maven](https://maven.apache.org/)

## About the author

**Piotr Jagodziński**

* [piotrjagod@poczta.onet.pl](mailto:piotrjagod@poczta.onet.pl)

## License
[MIT](https://opensource.org/licenses/mit-license.php)