package pl.sdacademy.users.controller;

import pl.sdacademy.users.config.JpaUtil;
import pl.sdacademy.users.entity.User;
import pl.sdacademy.users.service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = {"/users", "/addUser","/findUser", "/editUser","/deleteUser","/updateUser"})
public class UserController extends HttpServlet {

    private UserService userService;

    @Override
    public void init() {
        JpaUtil.getINSTANCE().getEntityManagerFactory();
        userService = new UserService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String actualPage = request.getServletPath();

        switch (actualPage) {
            case "/addUser":
                addUserGet(request, response);
                break;
            case "/findUser":
                findUserGet(request, response);
                break;
            case "/editUser":
                editUserGet(request, response);
                break;
            case "/updateUser":
                updateUserGet(request, response);
                break;
            case "/deleteUser":
                deleteUserGet(request, response);
                break;
            default:
                usersGet(request, response);
                break;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String actualPage = request.getServletPath();

        switch (actualPage) {
            case "/addUser":
                addUserPost(request, response);
                break;
            case "/findUser":
                findUserPost(request, response);
                break;
            case "/editUser":
                editUserPost(request, response);
                break;
            case "/updateUser":
                updateUserPost(request, response);
                break;
            case "/deleteUser":
                deleteUserPost(request, response);
                break;
            default:
                usersPost(request, response);
                break;
        }
    }

    @Override
    public void destroy() {
        JpaUtil.getINSTANCE().closeEntityManagerFactory();
    }

    private void usersGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<User> userList = userService.find();
        request.setAttribute("userList", userList);

        RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
        dispatcher.forward(request, response);
    }

    private void usersPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    private void findUserGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("findUser.jsp");
        dispatcher.forward(request, response);
    }

    private void findUserPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");

        List<User> userList = userService.findUserByName(firstName, lastName);

        request.setAttribute("userList", userList);

        RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
        dispatcher.forward(request, response);

    }

    private void addUserGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDispatcher dispatcher = request.getRequestDispatcher("editUser.jsp");
        dispatcher.forward(request, response);
    }

    private void addUserPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        User user = createUser(request, response);

        if (user.getEmail() != null) {
            userService.create(user);
        }

        response.sendRedirect("users");
    }

    private void editUserGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String editId = request.getParameter("id");
        if (editId != null) {
            User user = userService.findUserById(editId);
            RequestDispatcher dispatcher = request.getRequestDispatcher("editUser.jsp");
            request.setAttribute("user", user);
            dispatcher.forward(request, response);
            return;
        }

    }

    private void editUserPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    private void updateUserGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    private void updateUserPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        User user = createUser(request, response);
        String id = request.getParameter("id");
        user.setId(Long.parseLong(id));

        if (id != null) {
            userService.update(user);

            response.sendRedirect("users");
            return;
        }
    }

    private void deleteUserGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String deleteId = request.getParameter("id");
        if (deleteId != null) {
            userService.delete(deleteId);
            response.sendRedirect("users");
            return;
        }
        usersGet(request,response);
    }

    private void deleteUserPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    private User createUser(HttpServletRequest request, HttpServletResponse response ){
        User user = new User();
        user.setFirstName(request.getParameter("firstName"));
        user.setLastName(request.getParameter("lastName"));
        user.setEmail(request.getParameter("email"));
        user.setPassword(request.getParameter("password"));
        user.setZipCode(request.getParameter("zipCode"));
        user.setStreetName(request.getParameter("streetName"));
        user.setStreetNumber(request.getParameter("streetNumber"));
        user.setCity(request.getParameter("city"));

        return user;
    }
}
