package pl.sdacademy.users.controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/settings")
public class SettingsController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String langParam = request.getParameter("lang");
        if (langParam != null && !langParam.isEmpty()) {
            Cookie langCookie = new Cookie("lang", langParam);
            response.addCookie(langCookie);
        }

        response.sendRedirect("users");
//        String action = request.getServletPath();
//        response.sendRedirect(action);
    }
}
