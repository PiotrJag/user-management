package pl.sdacademy.users.repository;

import pl.sdacademy.users.config.JpaUtil;
import pl.sdacademy.users.entity.User;

import javax.persistence.EntityManager;
import java.util.List;

public class UserRepository {

    public List<User> findAll() {
        EntityManager em = JpaUtil.getINSTANCE().getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();

        List<User> users = em.createQuery("select u from User u", User.class).getResultList();

        em.getTransaction().commit();
        em.close();
        return users;
    }

    public List<User> findUserByName(String firstName, String lastName){
        EntityManager em = JpaUtil.getINSTANCE().getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();

        List<User> users =
                em.createQuery("select u from User u where u.firstName = :firstN and u.lastName = :lastN", User.class)
                .setParameter("firstN", firstName)
                .setParameter("lastN", lastName)
                .getResultList();

        em.getTransaction().commit();
        em.close();
        return users;
    }

    public User findUserById(Long id){
        EntityManager em = JpaUtil.getINSTANCE().getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();

        User user = em.find(User.class,id);

        em.getTransaction().commit();
        em.close();
        return user;
    }

    public void save(User user){
        EntityManager em = JpaUtil.getINSTANCE().getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();

        em.persist(user);

        em.getTransaction().commit();
        em.close();
    }

    public void update(User user){
        EntityManager em = JpaUtil.getINSTANCE().getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();

        em.merge(user);

        em.getTransaction().commit();
        em.close();
    }

    public void deleteById(Long id){
        EntityManager em = JpaUtil.getINSTANCE().getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();

        User user = em.find(User.class, id);
        em.remove(user);

        em.getTransaction().commit();
        em.close();
    }
}
