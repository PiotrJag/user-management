package pl.sdacademy.users.service;

import pl.sdacademy.users.entity.User;
import pl.sdacademy.users.repository.UserRepository;

import java.util.List;

public class UserService {

    private UserRepository userRepository = new UserRepository();

    public List<User> find() {
        return userRepository.findAll();
    }

    public List<User> findUserByName(String firstName, String lastName){
        return userRepository.findUserByName(firstName, lastName);
    }

    public User findUserById(String id){
        return userRepository.findUserById(Long.parseLong(id));
    }

    public void create(User user){
        userRepository.save(user);
    }

    public void update(User user){
        userRepository.update(user);
    }

    public void delete(String id){
        userRepository.deleteById(Long.parseLong(id));
    }

}
