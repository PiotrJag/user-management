package pl.sdacademy.users.config;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JpaUtil {

    private final static JpaUtil INSTANCE = new JpaUtil();
    private EntityManagerFactory emf;

    private JpaUtil() {
    }

    public static JpaUtil getINSTANCE() {
        return INSTANCE;
    }

    public EntityManagerFactory getEntityManagerFactory() {
        if (emf == null) {
            emf = Persistence.createEntityManagerFactory("users-db");
        }
        return emf;
    }

    public void closeEntityManagerFactory() {
        if (emf != null) emf.close();
    }
}
