Zadanie 1
=========

Utwórz aplikację zarządzającą użytkownikami. Aplikacja powinna zawierać następujące funkcjonalności:

- dodanie nowego użytkownika
- edycja istniejącego użytkownika
- usunięcie istniejącego użytkownika
- wyświetlenie listy wszystkich użytkowników
- wyszukiwanie użytkownika po imieniu i nazwisku

Dane użytkownika są następujące:

- imię
- nawisko
- adres email
- hasło
- ulica
- numer domu
- kod pocztowy
- miasto

