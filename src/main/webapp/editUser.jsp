<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<fmt:setLocale value="${!empty cookie.lang ? cookie.lang.value : param.lang}"/>
<fmt:setBundle basename="languages" var="languages"/>
<fmt:setBundle basename="labels"/>

<html>
<head>
    <title><fmt:message key="application.name"/></title>
    <link rel="stylesheet" href="webjars/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <nav class="navbar navbar-expand-sm bg-info navbar-dark fixed-top">

        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="users"><fmt:message key="users.allView"/></a>
            </li>
            <li class="nav-item">
                <c:if test="${user != null}">
                    <a class="nav-link" href="addUser"><fmt:message key="users.add"/></a>
                </c:if>
                <c:if test="${user == null}">
                    <a class="nav-link active" href="addUser"><fmt:message key="users.add"/></a>
                </c:if>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="findUser"><fmt:message key="users.find"/></a>
            </li>
        </ul>

    </nav>

    <div class="jumbotron jumbotron-fluid" style="height:135px">
        <div class="container text-center" style="margin-top:20px">
            <h2>
                <c:if test="${user != null}">
                    <fmt:message key="users.edit"/>
                </c:if>
                <c:if test="${user == null}">
                    <fmt:message key="users.add"/>
                </c:if>
            </h2>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <c:if test="${user != null}">
                <form action="updateUser" method="post">
            </c:if>
            <c:if test="${user == null}">
                <form action="addUser" method="post">
            </c:if>

                <c:if test="${user != null}">
                    <input type="hidden" name="id" value="<c:out value='${user.id}' />" />
                </c:if>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><fmt:message key="user.FirstName"/></span>
                    </div>
                    <input name="firstName" type="text" class="form-control" autocomplete="off" value="<c:out value='${user.firstName}' />">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><fmt:message key="user.LastName"/></span>
                    </div>
                    <input name="lastName" type="text" class="form-control" autocomplete="off" value="<c:out value='${user.lastName}' />">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><fmt:message key="user.Email"/></span>
                    </div>
                    <input name="email" type="text" class="form-control" autocomplete="off" value="<c:out value='${user.email}' />">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><fmt:message key="user.Password"/></span>
                    </div>
                    <input name="password" type="password" class="form-control" autocomplete="off" value="<c:out value='${user.password}' />">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><fmt:message key="user.StreetName"/></span>
                    </div>
                    <input name="streetName" type="text" class="form-control" autocomplete="off" value="<c:out value='${user.streetName}' />">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><fmt:message key="user.StreetNumber"/></span>
                    </div>
                    <input name="streetNumber" type="text" class="form-control" autocomplete="off" value="<c:out value='${user.streetNumber}' />">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><fmt:message key="user.ZipCode"/></span>
                    </div>
                    <input name="zipCode" type="text" class="form-control" autocomplete="off" value="<c:out value='${user.zipCode}' />">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><fmt:message key="user.City"/></span>
                    </div>
                    <input name="city" type="text" class="form-control" autocomplete="off" value="<c:out value='${user.city}' />">
                </div>
                <div class="input-group mb-3">
                    <button class="btn btn btn-primary btn-block" type="submit"><fmt:message key="user.save"/></button>
                </div>
            </form>

        </div>
        <div class="col-md-4"></div>
    </div>

</div>

<script src="webjars/jquery/3.0.0/jquery.min.js"></script>
<script src="webjars/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="webjars/popper.js/1.14.3/popper.min.js"></script>
</body>
</html>
