<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<fmt:setLocale value="${!empty cookie.lang ? cookie.lang.value : param.lang}"/>
<fmt:setBundle basename="languages" var="languages"/>
<fmt:setBundle basename="labels"/>

<html>
<head>
    <title><fmt:message key="application.name"/></title>
    <link rel="stylesheet" href="webjars/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <nav class="navbar navbar-expand-sm bg-info navbar-dark fixed-top">

        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link active" href="users"><fmt:message key="users.allView"/></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="addUser"><fmt:message key="users.add"/></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="findUser"><fmt:message key="users.find"/></a>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown"><fmt:message key="page.language"/></a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="settings?lang=en_GB"><fmt:message key="language.english" bundle="${languages}"/></a>
                    <a class="dropdown-item" href="settings?lang=de_DE"><fmt:message key="language.german" bundle="${languages}"/></a>
                    <a class="dropdown-item" href="settings?lang=pl_PL"><fmt:message key="language.polish" bundle="${languages}"/></a>
                </div>
            </li>
        </ul>
    </nav>

    <div class="jumbotron jumbotron-fluid" style="height:135px">
        <div class="container text-center" style="margin-top:20px">
            <h2 class="text-center"><fmt:message key="users.view"/></h2>
        </div>
    </div>

    <table class="table table-hover">
        <thead>
        <tr>
            <th>Id</th>
            <th><fmt:message key="user.FirstName"/></th>
            <th><fmt:message key="user.LastName"/></th>
            <th><fmt:message key="user.Email"/></th>
            <th><fmt:message key="user.StreetName"/></th>
            <th><fmt:message key="user.StreetNumber"/></th>
            <th><fmt:message key="user.ZipCode"/></th>
            <th><fmt:message key="user.City"/></th>
            <th><fmt:message key="user.actions"/></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="user" items="${requestScope.userList}">
            <tr>
                <td>${user.id}</td>
                <td>${user.firstName}</td>
                <td>${user.lastName}</td>
                <td>${user.email}</td>
                <td>${user.streetName}</td>
                <td>${user.streetNumber}</td>
                <td>${user.zipCode}</td>
                <td>${user.city}</td>
                <td><a href="editUser?id=<c:out value='${user.id}' />"><fmt:message key="actions.edit"/></a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="deleteUser?id=<c:out value='${user.id}' />"><fmt:message key="actions.delete"/></a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>


</div>

<script src="webjars/jquery/3.0.0/jquery.min.js"></script>
<script src="webjars/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="webjars/popper.js/1.14.3/popper.min.js"></script>
</body>
</html>
