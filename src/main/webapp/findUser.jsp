<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<fmt:setLocale value="${!empty cookie.lang ? cookie.lang.value : param.lang}"/>
<fmt:setBundle basename="languages" var="languages"/>
<fmt:setBundle basename="labels"/>

<html>
<head>
    <title><fmt:message key="application.name"/></title>
    <link rel="stylesheet" href="webjars/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <nav class="navbar navbar-expand-sm bg-info navbar-dark fixed-top">

        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="users"><fmt:message key="users.allView"/></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="addUser"><fmt:message key="users.add"/></a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="findUser"><fmt:message key="users.find"/></a>
            </li>
        </ul>

    </nav>

    <div class="jumbotron jumbotron-fluid" style="height:135px">
        <div class="container text-center" style="margin-top:20px">
            <h2 class="text-center"><fmt:message key="users.find"/></h2>
        </div>
    </div>


    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <form action="findUser" method="post">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><fmt:message key="user.FirstName"/></span>
                    </div>
                    <input name="firstName" type="text" class="form-control" autocomplete="off">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><fmt:message key="user.LastName"/></span>
                    </div>
                    <input name="lastName" type="text" class="form-control" autocomplete="off">
                </div>
                <div class="input-group mb-3">
                    <button class="btn btn btn-primary btn-block" type="submit"><fmt:message key="find"/></button>
                </div>
            </form>

        </div>
        <div class="col-md-4"></div>
    </div>

</div>

<script src="webjars/jquery/3.0.0/jquery.min.js"></script>
<script src="webjars/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="webjars/popper.js/1.14.3/popper.min.js"></script>
</body>
</html>
